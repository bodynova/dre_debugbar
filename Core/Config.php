<?php
namespace Bender\dre_DebugBar\Core;


use OxidEsales\Eshop\Core\Registry;

class Config extends Config_parent
{
    /**
     * @return array
     */
    public function getDebugBarConfigTrustedIps()
    {
        return (array) $this->getShopConfVar('debugbarTrustedIps', null, 'module:dre_debugbar') ?: [];
    }

    /**
     * @return string
     */
    public function getDebugBarConfigTheme()
    {
        return (string) $this->getShopConfVar('debugbarTheme', null, 'module:dre_debugbar') ?: 'default';
    }

    /**
     * @return int
     */
    public function getDebugBarConfigMaxProfiles(): int
    {
        return (int) $this->getShopConfVar('debugbarMaxProfiles', null, 'module:dre_debugbar') ?: 30;
    }

    /**
     * Gibt den Pfad zum Profile-Dir zurück, in welchem die gespeicherten Profile liegen
     *
     * @return string
     */
    public function getDebugBarProfileDir()
    {
        //die($this->getConfigParam('sCompileDir') . 'debugbar');
        //return Registry::getConfig()->getConfigParam('sShopDir').'/tmp/debugbar';
        //return Registry::getConfig()->getConfigParam('sCompileDir'). '/debugbar';
        return $this->getConfigParam('sCompileDir') . 'debugbar';
    }
}
