<?php
declare(strict_types=1);

namespace Bender\dre_DebugBar\Tests\Unit\Core\DebugBar\Elements;

use Bender\dre_DebugBar\Core\DebugBar\Elements\Modules;
use Bender\dre_DebugBar\Tests\UnitTestCase;

final class ModulesTest extends UnitTestCase
{
    /**
     *
     */
    public function testGetTitle()
    {
        /** @var Modules $tab */
        $tab = oxNew(Modules::class);

        $this->assertInternalType('string', $tab->getTitle());
    }

    /**
     *
     */
    public function testGetContent()
    {
        /** @var Modules $tab */
        $tab = oxNew(Modules::class);

        $this->assertInternalType('string', $tab->getContent());
    }
}
