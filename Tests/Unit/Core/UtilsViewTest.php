<?php
declare(strict_types=1);

namespace Bender\dre_DebugBar\Tests\Unit\Core;


use OxidEsales\Eshop\Core\Registry;
use OxidEsales\Eshop\Core\UtilsView;
use Bender\dre_DebugBar\Tests\UnitTestCase;

final class UtilsViewTest extends UnitTestCase
{
    /**
     *
     */
    public function testPolyfillFunctions()
    {
        /** @var \Bender\dre_DebugBar\Core\UtilsView $utils */
        $utils = oxNew(UtilsView::class);
        $utils->includeFunctions();

        $this->assertTrue(function_exists('getallheaders'));
    }

    /**
     *
     * @throws \ReflectionException
     */
    public function testIfSmartyPluginDirectoryIsAdded()
    {
        /** @var \Bender\dre_DebugBar\Core\UtilsView $utilsview */
        $utilsview = oxNew(UtilsView::class);

        /** @var \Smarty $smarty */
        $smarty = new \Smarty();

        $this->callMethod($utilsview, '_fillCommonSmartyProperties', [$smarty]);

        $smartyPluginsDir = Registry::getConfig()->getModulesDir() . "/bender/dre_debugbar/Core/Smarty/Plugin";

        $this->assertTrue(in_array($smartyPluginsDir, $smarty->plugins_dir));
    }
}
