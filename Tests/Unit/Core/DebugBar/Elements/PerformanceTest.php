<?php
declare(strict_types=1);

namespace Bender\dre_DebugBar\Tests\Unit\Core\DebugBar\Elements;

use Bender\dre_DebugBar\Core\DebugBar\Elements\Performance;
use Bender\dre_DebugBar\Tests\UnitTestCase;

final class PerformanceTest extends UnitTestCase
{
    /**
     *
     */
    public function testGetTitle()
    {
        /** @var Performance $tab */
        $tab = oxNew(Performance::class);

        $this->assertInternalType('string', $tab->getTitle());
    }

    /**
     *
     */
    public function testGetContent()
    {
        /** @var Performance $tab */
        $tab = oxNew(Performance::class);

        $this->assertInternalType('string', $tab->getContent());
    }
}
