<?php
/**
 * Smarty function
 * -------------------------------------------------------------
 * Purpose: Gibt die DebugBar aus
 * add [{fp_debug_bar}]
 * [{dre_debugbar_bar}]
 * -------------------------------------------------------------
 *
 * @param array  $aParams  parameters to process
 * @param \Smarty &$oSmarty smarty object
 *
 * @return string
 */
function smarty_function_dre_debugbar_bar($params, &$smarty)
{
    /** @var \Bender\dre_DebugBar\Core\DebugBar $debugBar */
    $debugBar = \OxidEsales\Eshop\Core\Registry::get(\Bender\dre_DebugBar\Core\DebugBar::class);
    $profile = $debugBar->getCurrentProfile();
    $id = $profile->getProfileId();

    $smarty->assign('debugbarProfileId', $id);
}
