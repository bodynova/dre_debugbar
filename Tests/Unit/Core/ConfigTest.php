<?php
declare(strict_types=1);

namespace Bender\dre_DebugBar\Tests\Unit\Core;


use Bender\dre_DebugBar\Core\DebugBar;
use OxidEsales\Eshop\Core\Config;
use OxidEsales\Eshop\Core\UtilsView;
use Bender\dre_DebugBar\Tests\UnitTestCase;

final class ConfigTest extends UnitTestCase
{
    /**
     *
     */
    public function testGetDebugBarConfigTrustedIps()
    {
        /** @var \Bender\dre_DebugBar\Core\Config $config */
        $config = oxNew(Config::class);

        $this->assertInternalType('array', $config->getDebugBarConfigTrustedIps());
    }

    /**
     *
     */
    public function testGetDebugBarConfigTheme()
    {
        /** @var \Bender\dre_DebugBar\Core\Config $config */
        $config = oxNew(Config::class);

        $this->assertInternalType('string', $config->getDebugBarConfigTheme());
    }
}
