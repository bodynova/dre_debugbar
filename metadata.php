<?php
$conf = \OxidEsales\Eshop\Core\Registry::getConfig();
$sModulesDir = $conf->getShopMainUrl().'modules/bender/dre_debugbar/';

/**
 * Metadata version
 */
$sMetadataVersion = '2.0';

/**
 * Module information
 */
$aModule = [
    'id'           => 'dre_debugbar',
    'title'        => '<img src="'.$sModulesDir.'out/img/favicon.ico" title="Bender Clear Temp Modul">ody DebugBar',
    'description'  => 'Integriert eine erweiterbare DebugBar in den Bodynova Shop, nach Vorbild des Symfony Web Profilers',
    'thumbnail'    => 'out/img/logo_bodynova.png',
    'version'      => '1.4.0',
    'author'       => 'André Bender',
    'url'          => 'https://bodynova.de',
    'email'        => 'support@bodynova.de',
    'extend'       => [
        \OxidEsales\Eshop\Core\UtilsView::class     => \Bender\dre_DebugBar\Core\UtilsView::class,

        \OxidEsales\Eshop\Core\Config::class        => \Bender\dre_DebugBar\Core\Config::class,

        \OxidEsales\Eshop\Core\ShopControl::class   => \Bender\dre_DebugBar\Core\ShopControl::class,

        \OxidEsales\Eshop\Core\Language::class      => \Bender\dre_DebugBar\Core\Language::class,
    ],

    'controllers'           => [
        //source
        'dre_debugbar_getprofile' => \Bender\dre_DebugBar\Application\Controller\GetProfileController::class,
    ],

    'events'                => [
        'onActivate'        =>  '\Bender\dre_DebugBar\Core\Events::onActivate',
        'onDeactivate'      =>  '\Bender\dre_DebugBar\Core\Events::onDeactivate',
    ],

    'templates'             => [
    ],

    'blocks'                => [
        [
            'template'      => 'layout/base.tpl',
            'block'         => 'base_js',
            'file'          => 'Application/views/tpl/layout/base/js.tpl',
        ]
    ],

    'settings' => [
        [
            'group'         => 'debugbarMain',
            'name'          => 'debugbarTheme',
            'type'          => 'select',
            'value'         => 'default',
            'constraints'   => 'default|forrest|plum|fortressofsolitude|herbs',
        ],
        [
            'group'         => 'debugbarMain',
            'name'          => 'debugbarTrustedIps',
            'type'          => 'arr',
            'value'         => [],
        ],
        [
            'group'         => 'debugbarMain',
            'name'          => 'debugbarMaxProfiles',
            'type'          => 'str',
            'value'         => 30,
        ],
    ],
];
