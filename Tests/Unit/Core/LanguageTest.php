<?php
declare(strict_types=1);

namespace Bender\dre_DebugBar\Tests\Unit\Core;


use Bender\dre_DebugBar\Core\DebugBar;
use OxidEsales\Eshop\Core\Config;
use OxidEsales\Eshop\Core\Registry;
use OxidEsales\Eshop\Core\UtilsView;
use Bender\dre_DebugBar\Tests\UnitTestCase;

final class LanguageTest extends UnitTestCase
{
    /**
     *
     */
    public function testGetDebugbarMissingTranslations()
    {
        /** @var \Bender\dre_DebugBar\Core\Language $lang */
        $lang = Registry::getLang();

        // Translation-Error triggern
        $lang->translateString('UNITTEST_NOT_TRANSLATED_STRING_LANGUAGETEST');

        $this->assertInternalType('array', $lang->getDebugbarMissingTranslations());
        $this->assertEquals(1, count($lang->getDebugbarMissingTranslations()));
    }
}
