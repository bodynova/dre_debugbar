<?php
declare(strict_types=1);

namespace Bender\dre_DebugBar\Tests\Unit\Core;


use Bender\dre_DebugBar\Core\DebugBar;
use OxidEsales\Eshop\Core\Config;
use OxidEsales\Eshop\Core\UtilsView;
use Bender\dre_DebugBar\Tests\UnitTestCase;

final class EventsTest extends UnitTestCase
{
    /**
     *
     */
    public function testOnActivate()
    {
       $this->markTestIncomplete();
    }

    /**
     *
     */
    public function testOnDeactivate()
    {
        $this->markTestIncomplete();
    }

    /**
     *
     */
    public function testClearCache()
    {
        $this->markTestIncomplete();
    }

    /**
     *
     */
    public function testReloadSmarty()
    {
        $this->markTestIncomplete();
    }
}
