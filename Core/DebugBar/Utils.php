<?php
namespace Bender\dre_DebugBar\Core\DebugBar;

use OxidEsales\Eshop\Core\Base;
class Utils extends Base
{
    /**
     * @return string|null
     */
    public function getUserIp()
    {
        return $_SERVER['REMOTE_ADDR'] ?: null;
    }
}
