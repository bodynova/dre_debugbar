<?php
declare(strict_types=1);

namespace Bender\dre_DebugBar\Tests\Unit\Core\DebugBar\Tabber;

use Bender\dre_DebugBar\Core\DebugBar\Tabber;
use Bender\dre_DebugBar\Tests\TestData;
use Bender\dre_DebugBar\Tests\UnitTestCase;

final class TabTest extends UnitTestCase
{
    /**
     *
     */
    public function testGetContent()
    {
        $tab = TestData::Tab();

        $this->assertEquals('Unittests Content', $tab->getContent());
    }

    /**
     *
     */
    public function testGetTitle()
    {
        $tab = TestData::Tab();

        $this->assertEquals('Unittests Title', $tab->getTitle());
    }

    /**
     *
     */
    public function testGetKey()
    {
        $tab = TestData::Tab();

        $this->assertEquals('_unittests_tab', $tab->getKey());
    }
}
